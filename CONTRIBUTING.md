# Contribution guidelines

## Reporting bugs, requesting features, and asking questions

Please use the [Issue Tracker](https://gitlab.com/grosscra/zipf/-/issues).
Make sure to first search through open and closed issues to see if your issue has been addressed.
For
- Bugs: use the "Bug" label
- Features: use the "Enhancement" label
- Questions: use the "Question" label

## Contributing code

Please issue a [merge request](https://gitlab.com/grosscra/zipf/-/merge_requests).

## Note

This is a test project from the book [Research Software Engineering with Python](https://merely-useful.tech/py-rse/) and does not actually need or accept contributions
