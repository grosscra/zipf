.PHONY : all clean help results settings

include config.mk

DATA=$(wildcard data/*.txt)
RESULTS=$(patsubst data/%.txt,results/%.csv,$(DATA))

## all : Regenerate all results.
all : results/collated.png

## results/collated.png: Plot the collated results.
results/collated.png: results/collated.csv $(PLOT_PARAMS) $(PLOT)
	python $(PLOT) $< --outfile $@ --plotparams $(word 2,$^)

## results/collated.csv : Collate all results.
results/collated.csv : $(RESULTS) $(COLLATE)
	mkdir -p results
	python $(COLLATE) $(RESULTS) > $@

## results : Make results csv files without collating
results : $(RESULTS)

## results/%.csv : Regenerate results for all books
results/%.csv : data/%.txt $(COUNT) $(SUMMARIZE)
	mkdir -p results
	@bash $(SUMMARIZE) $< Title
	@bash $(SUMMARIZE) $< Author
	python $(COUNT) $< > $@

## test-saveconfig : Save configuration variables
test-saveconfig : results/collated.csv $(PLOT_PARAMS) $(PLOT)
	python $(PLOT) $< --outfile results/collated.png --plotparams $(word 2,$^) --saveconfig test-saveconfig.yml

## clean : Remove all generated files.
clean : 
	rm -f $(RESULTS) results/collated.csv results/collated.png test-saveconfig.yml collate.log

## settings : Show variables' values.
settings:
	@echo COUNT: $(COUNT)
	@echo COLLATE: $(COLLATE)
	@echo PLOT_PARAMS: $(PLOT_PARAMS)
	@echo PLOT: $(PLOT)
	@echo SUMMARIZE: $(SUMMARIZE)
	@echo DATA: $(DATA)
	@echo RESULTS: $(RESULTS)

## help : Show this message.
help :
	@grep -h -E '^##' ${MAKEFILE_LIST} | sed -e 's/## //g' \
	| column -t -s ':'
