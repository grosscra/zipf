"""
Count the occurences of punctuation in text.
"""

import argparse
import string
from collections import Counter


def count_punctuation(reader):
    """
    Count the occurrence full stops, question marks, and exclamation points
    in input text.

    Parameters
    ----------
    reader
        text stream

    Returns
    -------
    Counter
        number of occurrences of each punctuation

    """
    punctuation_list = ['.', '?', '!']
    text = reader.read()
    punctuation_counts = Counter()
    for punctuation in punctuation_list:
        punctuation_counts[punctuation] = text.count(punctuation)
    return punctuation_counts


def main(args):
    """Run the command line program."""
    punct_counts = count_punctuation(args.infile)
    for punctuation, count in punct_counts.items():
        print(f'Number of {punctuation} is {count}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('infile', type=argparse.FileType('r'),
                        nargs='?', default='-',
                        help='Input file name')
    args = parser.parse_args()
    main(args)
