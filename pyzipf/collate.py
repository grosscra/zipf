"""
Combine multiple word count CVS-files
into a single cumulative count.
"""

import csv
import argparse
import logging
from collections import Counter

from pyzipf import utilities as util


def collection_to_csv(collection, num=None):
    """Write collection of items and counts in csv format."""
    collection = collection.most_common()
    if num is None:
        num = len(collection)
    writer = csv.writer(sys.stdout)
    writer.writerows(collection[0:num])


def update_counts(reader, word_counts):
    """Update word counts with data from another reader/file."""
    for word, count in csv.reader(reader):
        word_counts[word] += int(count)


def process_file(fname, word_counts):
    """Read file and update word counts"""
    logging.debug(f'Reading in {fname}...')
    if fname[-4:] != '.csv':
        msg = util.ERRORS['not_csv_suffix'].format(fname=fname)
        raise OSError(msg)
    with open(fname, 'r') as reader:
        logging.debug('Computing word counts...')
        update_counts(reader, word_counts)


def parse_command_line():
    """Parse the commandline for input arguments."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('infiles', type=str, nargs='*',
                        help='Input file names')
    parser.add_argument('-n', '--num',
                        type=int, default=None,
                        help='Output n most frequent words')
    parser.add_argument('-l', '--logfile',
                        type=str, default='collate.log',
                        help='Set file to store logs')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Display debug logging')
    args = parser.parse_args()
    return args


def main():
    """Run the commmand line program."""
    args = parse_command_line()
    logging_level = logging.DEBUG if args.verbose else logging.WARNING
    logging.basicConfig(level=logging_level, filename=args.logfile)
    word_counts = Counter()
    logging.info('Processing files...')
    for fname in args.infiles:
        try:
            process_file(fname, word_counts)
        except FileNotFoundError:
            error = 'Not found.'
            msg = util.ERRORS['file_open_error'].format(fname=fname,
                                                        exception=error)
            logging.warning(msg)
        except PermissionError:
            error = 'Incorrect permission.'
            msg = util.ERRORS['file_open_error'].format(fname=fname,
                                                        exception=error)
            logging.warning(msg)
        except Exception as error:
            msg = util.ERRORS['file_open_error'].format(fname=fname,
                                                        exception=error)
            logging.warning(msg)
    util.collection_to_csv(word_counts, num=args.num)


if __name__ == '__main__':
    main()
