"""Collection of commonly used functions."""

import sys
import csv


ERRORS = {
    'not_csv_suffix': '{fname}: File must end in .csv',
    'file_open_error':
        '{fname}: Cannot open file for reading, skipping: {exception}'
}


def collection_to_csv(collection, num=None):
    """Write collection of items and counts in csv format.

    Parameters
    ----------
    collection
        A collection of items.
    num : int, optional
        Number of words to write.
    """
    collection = collection.most_common()
    if num is None:
        num = len(collection)
    writer = csv.writer(sys.stdout)
    writer.writerows(collection[0:num])

